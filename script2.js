function opcion(humano){
    const TextoResultado = ["Empate","Ganas","Pierdes"];
    const nombre = ["Piedra","Papel","Tijeras","Lagarto","Spock"];

    const jugar = [
                    [0,1,2,2,1],
                    [2,0,1,1,2],
                    [1,2,0,2,1],
                    [1,2,1,0,2],
                    [2,1,2,1,0]
    ];

    const cpu = Math.floor(Math.random() *5);

    resultado = jugar[cpu][humano];
    console.log("Humano: " + nombre[humano]);
    console.log("CPU: " + nombre[cpu]);
    console.log("El resultado es:" + TextoResultado[resultado]);

    let mensaje1 = new SpeechSynthesisUtterance("Humano: " + nombre[humano]);
    let mensaje2 = new SpeechSynthesisUtterance("CPU: " + nombre[cpu]);
    let mensaje3 = new SpeechSynthesisUtterance(TextoResultado[resultado]);

    window.speechSynthesis.speak(mensaje1);
    window.speechSynthesis.speak(mensaje2);
    window.speechSynthesis.speak(mensaje3);
}

