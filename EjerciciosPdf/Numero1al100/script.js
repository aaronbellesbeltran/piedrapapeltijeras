/* 1.Escribe un programa que use que imprima por pantalla los números de 1 a 100, con
dos excepciones. Los números que sean divisibles por 3, no aparecerán, en su lugar
aparecerá́“Fizz” y para los números divisibles por 5 aparecerá́“Buzz”. Los que sean
divisibles por 3 y por 5 aparecerá́“FizzBuzz”. */


for(let i=1;i<=100;i++){
    if(i%3==0 || i%5==0){
        if(i%3 == 0 && i%5 == 0){
            console.log("FizzBuzz");
        }else if(i%3 == 0){
            console.log("Fizz");
        }else if(i%5 == 0){
            console.log("Buzz");
        }    
    }else console.log(i);
}