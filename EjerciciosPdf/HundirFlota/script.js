/* Crea un vector de 10 posiciones en el que, inicialmente, todaslas posiciones sean
‘X’ (x mayúscula). Luego, de forma aleatoria, escoge dos de las posiciones del
vector y cámbialas por ‘O’ (o mayúscula). El objetivo del programa es que el
usuario encuentre esas dos posiciones, que son desconocidas para él, dentro del
vector.
Para ello el programa le pedirá al usuario dos posiciones del vector (dos números
enteros) y le contestará el número de posiciones que ha acertado. Si son las dos
le dirá que ha ganado, y si no le volverá a pedir las dos posiciones. Así hasta que
acierte, pero con la limitación de que sólo puede tener 5 oportunidades. Es decir,
el juego acabaría cuando acertara o si fallara 5 veces. */

let vector= [];


function crearPosiciones(v){
    for(i=0;i<10;i++){
        v.push("X");
    }    
}

function mostrarVector(v){
    for(i=0;i<v.length;i++){
        console.log("La posicion :v",i,"equivale a :"+v[i]);
    }
}

function anyadirO(v){
    let contador = 0;
    do{
        let numeroAleatorio = Math.floor(Math.random() * (9 - 0) + 0);
        if(v[numeroAleatorio]=="X"){
            v[numeroAleatorio] = "O";
            contador++;
        }        
    }while(!(contador==2))
}

function jugar(v){
    let respuestasCorrectas = 0;
    let maxTiradas = 5;
    do{
        //Va a hacer 2 preguntas(prompt) y ver la posicion. Si es correcta respuestacorrecta++ y maxTiradas--
        
    }
    while(!(respuestasCorrectas == 2) || (maxTiradas ==0))
}

crearPosiciones(vector);
mostrarVector(vector);
anyadirO(vector);
mostrarVector(vector);
