let allElementos = document.querySelector("li");
let primerElemento = document.querySelector(".primerElemento");
let segundoElemento = document.querySelector("#segundoElemento");

primerElemento.textContent = "Elemento modificado desde el DOM con la class";
segundoElemento.textContent = "Segundo elemento modificado desde el DOM con el id";

const nuevoElemento = document.createElement("li");
nuevoElemento.textContent ="Creado nuevo elemento desde el DOM";
const lista = document.querySelector("#lista");
lista.appendChild(nuevoElemento);

