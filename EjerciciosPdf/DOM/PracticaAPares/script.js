/* Crea un documento HTML que conste de un título h1 con el texto “Ejemplo de Javascript con
eventos”, dos párrafos de texto, en los cuales tiene que haber dos enlaces, uno en cada
párrafo, por ejemplo y una imagen.
Crea un archivo de Javascript para:
- Utilizar los eventos onmouseover para que la imagen original sea reemplazada por
otra imagen cuando el usuario pasa el ratón sobre ella. Utiliza el evento onmouseout
para hacer que cuando el usuario salga del espacio de la imagen, vuelva a aparecer la
imagen original.
- al entrar en la página nos muestre un mensaje de bienvenida tipo: “Hola, bienvenido a
mi página”.
- y para que cuando hagas clic en los enlaces cambie el color de estos a rojo. */

document.querySelector("#img1").addEventListener("mouseover",cambiaImagen);
document.querySelector("#img1").addEventListener("mouseout",cambiaImagen);
document.querySelectorAll(".enlace").forEach(element => 
    element.addEventListener("click", () => element.classList.add("rojo"))
  );
  
const cambiaImagen = () =>{
    if (this.src=="imagen1.jpg") this.src="imagenAlterna.png";
    else this.src=="imagen1.jpg";
}

/* document.querySelectorAll("p").forEach(element =>
    element.addEventListener()) */
/* function cambiaImagen(){
    console.log(document.querySelector("#img1").src);
    if (document.querySelector("#img1").src == 'imagen1.jpg') { document.querySelector("#img1").src= "imagenAlterna.png";}
    else document.querySelector("#img1").src = "imagen1.jpg";
} */


/* document.body.onload = function() {
    alert("Hola, bienvenido a mi página");
  } */