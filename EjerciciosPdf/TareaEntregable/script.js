const ejemplo = [23, 48, 'xarxatec', undefined, 7, null, 88888888];

esPar(ejemplo);

function esPar(v){
    let cont=0;
    for(let i=0;i<v.length;i++){        
        if(typeof(v[i])== 'number'){
            let suma = parseInt(v[i]).toString().split('').reduce((total, actual) => total + +actual, 0); 
            if(suma%2==0) cont++;
        }
    }
    return console.log("El número de valores cuya suma de dígitos es par es: ", cont);
}