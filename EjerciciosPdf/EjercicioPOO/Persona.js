class Persona{  
    constructor(nombre, primerApellido, fecha){
        this._nombre = nombre;
        this._primerApellido = primerApellido;
        this._fechaNacimiento = fecha;
    }

    get nombre(){
        return this._nombre;
    }

    set nombre(nombreNuevo){
        this._nombre = nombreNuevo;
    }

    get primerApellido(){
        return this._primerApellido;
    }

    set primerApellido(nuevoPrimerApellido){
        this._primerApellido = nuevoPrimerApellido;
    }

    get fechaNacimiento(){
        return this._fechaNacimiento;
    }

    set fechaNacimiento(nuevaFecha){
        this._fechaNacimiento = nuevaFecha;
    }

    saluda(){
        console.log(`Hola, soy ${this.nombre} ${this.primerApellido} `);
    }
    despidete(){
        console.log(`Adeu!`);
    }
  
}


export {Persona};