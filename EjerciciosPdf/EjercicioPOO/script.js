import {Persona} from "./Persona.js";
class Cuenta{
    constructor(titular,cantidad){
        this._titular=titular;
        this._cantidad=cantidad;
    }
    get titular(){
        return this._titular;
    }
    set titular(nuevoTitular){
        this._titular = nuevoTitular;
    }
    get cantidad(){
        return this._cantidad;
    }
    set cantidad(nuevaCantidad){
        this._cantidad = nuevaCantidad;
    }
    mostrar(){
        console.log(this._titular,this._cantidad);
    }
    ingresar(x){
        this._cantidad+= x;
        this.mostrar();
    }
    retirar(x){
        this._cantidad -= x;
        this.mostrar();
    }
}
class cuentaExpansion extends Cuenta{
    constructor(titular,cantidad,minimoIngresoMensual){
        super(titular,cantidad);
        this._minimoIngresoMensual = minimoIngresoMensual;
    }
    get minimoIngresoMensual(){
        return this._minimoIngresoMensual;
    }
    set minimoIngresoMensual(nuevoMinimoIngresoMensual){
        this._minimoIngresoMensual = nuevoMinimoIngresoMensual;
    }
    transferencia(numeroCuenta,ingreso){
        this._cantidad -= ingreso;
        console.log("Se ingresó en ",numeroCuenta, "la cantidad ",ingreso);
    }
}
class CuentaPersonal extends Cuenta{

}
class CuentaJoven extends Cuenta{
    constructor(titular,cantidad, bonificacion){
        super(titular, cantidad);
        this._bonificacion = bonificacion;
    }
    get bonificacion(){
        return this._bonificacion;
    }
    set bonificacion(nuevaBonificacion){
        this._bonificacion = nuevaBonificacion;
    }
    esTitularValido(){
        console.log(this.titular.fechaNacimiento);
    }
}
let nuevaCuenta= new Cuenta("aaron",150);
nuevaCuenta.mostrar();
nuevaCuenta.ingresar(50);
nuevaCuenta.retirar(50);

let nuevaPersona = new Persona("Aaron","Belles","02/21/1993");

let CuentaJoven1 = new CuentaJoven(nuevaPersona,1000, 2);
CuentaJoven1.esTitularValido();