/* 3. Escribe un programa que cree una cadena que represente una red 8x8, usando el
carácter de nueva línea para separar las líneas. Cada posición de la red será́o bien
“#” o un espacio en blanco “ “. Todos los caracteres deberían formar un tablero de
ajedrez. Si lo pasásemos por console.log debería mostrar lo siguiente: 
# # # #
# # # #
# # # #
# # # #
# # # #
# # # #
# # # #
# # # #
*/
let tamanyo = 8;
let textoImprimir = "";
for(let i=1;i<=tamanyo;i++){
    for(let j=1;j<=tamanyo;j++){
        if(i%2 ==0){
            textoImprimir+= "# ";
        }
        else{
            textoImprimir+= " #";
    }    
}
    console.log(textoImprimir);
    textoImprimir="";
}